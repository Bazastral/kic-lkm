

# Kernel module - keyboard interrupt

Kernel module that counts interrupts from keybord.

# Build & run
To build simply run `make`.\
Then `make start` to insert module to the kernel.\
Next, `mknod /dev/kic c <major number> 0`. Note that "major number" will be printed in previous command.\
Then, you can use `sudo ./kic_us`. This will print 3 possible actions.\
When you are done, remove the module using `make stop`.

# More about the module
Module with US (User Space) application are communicating through character devic
e file `/dev/kic`. Module shows there two infos: Interrupt Counter & when it was
reset (in Linux Epoch). User space application `./kic_us` gets them by opening
and reading from the device. You can access them also by `sudo cat /dev/kic`.\\
Reseting counter works in other direction. The app has to open the file and
write to it one character: 'r'. You can also do it from bash: `sudo echo "r" > /dev/kic`.
