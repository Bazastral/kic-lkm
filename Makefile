


obj-m += lkm.o
all:
	 gcc kic_us.c -o kic_us
	 make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	 make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
	 rm kic_us
start:
	 sudo dmesg -C
	 sudo insmod lkm.ko
	 dmesg
stop:
	 sudo rm /dev/kic
	 sudo rmmod lkm.ko
