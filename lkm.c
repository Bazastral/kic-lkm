




#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/init.h>
#include <linux/time.h>
#include <linux/interrupt.h>
#include <linux/signal.h>
#include <asm/io.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Bazyli Gielniak");
MODULE_DESCRIPTION("A simple example Linux module"
    " which counts intterupts from keyboard");
MODULE_VERSION("0.0.1");


#define SUCCESS 0
#define DEVICE_NAME "kic"
#define MAX_BUF_SIZE 80    //Max length of the message from the device

static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);
static void reset_interrupt_cnt(void);

static int Major;              //Major number assigned to our device driver
static bool is_opened = false; //Used to prevent multiple access to device
static char msg[MAX_BUF_SIZE];
static char *msg_Ptr;
static long long int interrupt_cnt;
static long long int last_reset;

static struct file_operations fops = {
  .read = device_read,
  .write = device_write,
  .open = device_open,
  .release = device_release
};

static int __init lkm_init(void){
  struct timeval tv;

  Major = register_chrdev(0, DEVICE_NAME, &fops);
  if (Major < 0) {
    printk(KERN_ALERT "Registering char device failed with %d\n", Major);
    return Major;
  }
  do_gettimeofday(&tv);
  last_reset = (long long int) tv.tv_sec;
  printk(KERN_INFO "Create the char device file to communicate with module:\n");
  printk(KERN_INFO ">  sudo mknod /dev/%s c %d 0\n", DEVICE_NAME, Major);
  printk(KERN_INFO "\n");
  return SUCCESS;
}

static void __exit lkm_exit(void){
  printk(KERN_INFO "Exiting!\n");
  //Unregister the device
  unregister_chrdev(Major, DEVICE_NAME);
}

//***************************************************************************
//Methods
//***************************************************************************

static int device_open(struct inode *inode, struct file *file){
  //only one process at one time
  if(is_opened)
    return -EBUSY;

  is_opened = true;
  //prepare message
  snprintf(msg, MAX_BUF_SIZE,
      "Keyboard interrupt counter: %lld\n"
      "Last open: %lld\n",
      interrupt_cnt, last_reset);
  msg_Ptr = msg;
  try_module_get(THIS_MODULE);
  //TODO
  //increment counter once a device is opened.
  //This should happen in interrupt handler function.
  interrupt_cnt++;
  return SUCCESS;
}

//Called when a process closes the device file.
static int device_release(struct inode *inode, struct file *file){
  is_opened = false;
  module_put(THIS_MODULE);
  return 0;
}


//Called when a process, which already opened the dev file, attempts to
//read from it.
static ssize_t device_read(struct file *filp, char *buffer,
    size_t length, loff_t * offset)
{
  int bytes_read = 0;
  //If we're at the end of the message,
  //return 0 signifying end of file
  if (*msg_Ptr == 0)
    return 0;

  //Actually put the data into the buffer
  while (length && *msg_Ptr) {
    put_user(*(msg_Ptr++), buffer++);
    length--;
    bytes_read++;
  }
  return bytes_read;
}

//Steering this module is only through this function
static ssize_t device_write(struct file *filp, const char *buff,
    size_t size, loff_t * offset)
{
#define MAX_NUM_LETTERS 1
  char letters[MAX_NUM_LETTERS+1];
  int len;

  if(size > MAX_NUM_LETTERS+1)
    printk(KERN_ALERT "Only one char can be written.\n"
        "Will check first char.\n");
  len = min(size, (size_t) (MAX_NUM_LETTERS+1));

  if (copy_from_user(letters, buff, len))
    return -EFAULT;

  if(letters[0] == 'r')
    reset_interrupt_cnt();
  return len;
}

static void reset_interrupt_cnt(void)
{
  struct timeval tv;
  interrupt_cnt = 0;
  do_gettimeofday(&tv);
  last_reset = (long long int) tv.tv_sec;
  printk(KERN_INFO "Counter reset\n");
}


module_init(lkm_init);
module_exit(lkm_exit);
