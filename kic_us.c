



#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define DEVICE "/dev/kic"
#define ARG_RESET             "reset"
#define ARG_GET_CNT           "get_cnt"
#define ARG_GET_RESET_DATE    "get_reset_date"
#define DRIVER_CNT_STRING     "Keyboard interrupt counter: "
#define DRIVER_DATE_STRING    "Last open: "

#define SUCCESS 0
#define ERROR 1
#define MAX_READ_BUF_LEN 100

int usage(void);
int reset(void);
int get_cnt(void);
int get_rst_date(void);



int main(int argc, char** argv)
{
  printf("Hello to Keyboard Interrupt Counter - "
      "User Space application! \n");
  if(argc != 2)
  {
    return usage();
  }

  if(!strcmp(argv[1], ARG_RESET))
  { return reset(); }
  else if(!strcmp(argv[1], ARG_GET_CNT))
  { return get_cnt(); }
  else if(!strcmp(argv[1], ARG_GET_RESET_DATE))
  { return get_rst_date(); }
  else
  {
    printf("Wrong argument!\n");
    return usage();
  }

  return SUCCESS;
}

int reset(void)
{
  int fd = open(DEVICE,O_WRONLY);
  if(fd == -1)
    if(open(DEVICE,O_WRONLY) == -1)
    { printf("Error while opening device file %s", DEVICE); return ERROR;}

  char write_buf[] = "r";
  write(fd, write_buf, sizeof(write_buf));
  printf("Reset performed\n");
  close(fd);
  return SUCCESS;
}

int get_cnt(void)
{
  int fd = open(DEVICE,O_RDONLY);
  if(fd == -1)
  { printf("Error while opening device file %s", DEVICE); return ERROR;}

  char read_buf[MAX_READ_BUF_LEN];
  long long int cnt;
  read(fd, read_buf, MAX_READ_BUF_LEN);
  close(fd);
  if(sscanf(read_buf, DRIVER_CNT_STRING"%lld", &cnt) == 1)
  {
    printf("Interrupt counter = %lld\n", cnt);
    return SUCCESS;
  }
  else
  {
    printf("Some error occured\n");
    return ERROR;
  }
}


int get_rst_date(void)
{
  int fd = open(DEVICE,O_RDONLY);
  if(fd == -1)
  { printf("Error while opening device file %s", DEVICE); return ERROR;}

  char read_buf[MAX_READ_BUF_LEN];
  time_t epoch;
  read(fd, read_buf, MAX_READ_BUF_LEN);
  close(fd);
  if(sscanf(strchr(read_buf, '\n')+1,
        DRIVER_DATE_STRING"%ld", &epoch) == 1)
  {
    printf("Last counter reset occured:\n"
        "Unix epoch: %ld\n"
        "Date: %s\n",
        epoch, asctime(gmtime(&epoch)));
    return SUCCESS;
  }
  else
  {
    printf("Some error occured\n");
    return ERROR;
  }
}

int usage(void)
{

  printf("Usage:\n"
      "kic_us [%s|%s|%s]\n",
      ARG_RESET,
      ARG_GET_CNT,
      ARG_GET_RESET_DATE);
  return SUCCESS;
}


